package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JToggleButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JColorChooser;

public class Form_�ndern extends JFrame {

	private JPanel contentPane;
	private JLabel lblHierBitteText;
	private JTextField txtHierBitteText;
	private JButton btnInsLabelSchreiben;
	private JButton btnTextImLabelL�schen;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Form_�ndern frame = new Form_�ndern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Form_�ndern() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 587);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAufgabeHintergrundfarbe = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblAufgabeHintergrundfarbe.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAufgabeHintergrundfarbe.setBounds(10, 63, 364, 14);
		contentPane.add(lblAufgabeHintergrundfarbe);
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonRot_clicked();
			}
		});
		btnRot.setBounds(10, 88, 103, 23);
		contentPane.add(btnRot);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonBlau_clicked();
			}
		});
		btnBlau.setBounds(259, 88, 115, 23);
		contentPane.add(btnBlau);
		
		JButton btnGrn = new JButton("Gr\u00FCn");
		btnGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonGr�n_clicked();
			}
		});
		btnGrn.setBounds(123, 88, 126, 23);
		contentPane.add(btnGrn);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonGelb_clicked();
			}
		});
		btnGelb.setBounds(10, 122, 103, 23);
		contentPane.add(btnGelb);
		
		JButton btnStandardfarbe = new JButton("Standardfarbe");
		btnStandardfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonStandart_clicked();
			}
		});
		btnStandardfarbe.setBounds(122, 122, 126, 23);
		contentPane.add(btnStandardfarbe);
		
		JButton btnFarbeWhlen = new JButton("Farbe w\u00E4hlen");
		btnFarbeWhlen.setEnabled(false);
		btnFarbeWhlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonWahl_clicked();
			}
		});
		btnFarbeWhlen.setBounds(259, 122, 115, 23);
		contentPane.add(btnFarbeWhlen);
		
		JLabel lblAufgabeTest = new JLabel("Aufgabe 2: Test formatieren");
		lblAufgabeTest.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAufgabeTest.setBounds(10, 156, 364, 14);
		contentPane.add(lblAufgabeTest);
		
		lblHierBitteText = new JLabel("Hier bitte Text eingeben");
		lblHierBitteText.setHorizontalAlignment(SwingConstants.CENTER);
		lblHierBitteText.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblHierBitteText.setBounds(10, 11, 364, 41);
		contentPane.add(lblHierBitteText);
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonArial_clicked();
			}
		});
		btnArial.setBounds(10, 181, 103, 23);
		contentPane.add(btnArial);
		
		JButton btnComicSansMs = new JButton("Comic Sans MS");
		btnComicSansMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonComicSansMS_clicked();
			}
		});
		btnComicSansMs.setBounds(123, 181, 126, 23);
		contentPane.add(btnComicSansMs);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCourierNew_clicked();
			}
		});
		btnCourierNew.setBounds(259, 181, 115, 23);
		contentPane.add(btnCourierNew);
		
		txtHierBitteText = new JTextField();
		txtHierBitteText.setText("Hier bitte Text eingeben");
		txtHierBitteText.setBounds(10, 216, 364, 20);
		contentPane.add(txtHierBitteText);
		txtHierBitteText.setColumns(10);
		
		btnInsLabelSchreiben = new JButton("Ins Label schreiben");
		btnInsLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textAusTextfeldEinlesen(txtHierBitteText.getText());
			}
		});
		btnInsLabelSchreiben.setBounds(10, 247, 165, 23);
		contentPane.add(btnInsLabelSchreiben);
		
		btnTextImLabelL�schen = new JButton("Text im Label L\u00F6schen");
		btnTextImLabelL�schen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textAusTextfeldEinlesen("");
			}
		});
		btnTextImLabelL�schen.setBounds(185, 247, 189, 23);
		contentPane.add(btnTextImLabelL�schen);
		
		JLabel lblAufgabeSchriftfarbe = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblAufgabeSchriftfarbe.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAufgabeSchriftfarbe.setBounds(10, 282, 364, 14);
		contentPane.add(lblAufgabeSchriftfarbe);
		
		JButton btnRot_1 = new JButton("Rot");
		btnRot_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				farbe�ndern(Color.RED);
			}
		});
		btnRot_1.setBounds(10, 307, 103, 23);
		contentPane.add(btnRot_1);
		
		JButton btnBlau_1 = new JButton("Blau");
		btnBlau_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				farbe�ndern(Color.BLUE);
			}
		});
		btnBlau_1.setBounds(123, 307, 126, 23);
		contentPane.add(btnBlau_1);
		
		JButton btnSchwarz = new JButton("Schwarz");
		btnSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				farbe�ndern(Color.BLACK);
			}
		});
		btnSchwarz.setBounds(259, 307, 115, 23);
		contentPane.add(btnSchwarz);
		
		JLabel lblAufgabeSchriftgre = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblAufgabeSchriftgre.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAufgabeSchriftgre.setBounds(10, 342, 364, 14);
		contentPane.add(lblAufgabeSchriftgre);
		
		JButton button = new JButton("+");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				groe�eAendern(true);
			}
		});
		button.setBounds(10, 363, 165, 23);
		contentPane.add(button);
		
		JButton button_1 = new JButton("-");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				groe�eAendern(false);
			}
		});
		button_1.setBounds(185, 363, 189, 23);
		contentPane.add(button_1);
		
		JLabel lblAufgabeTextausrichtung = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabeTextausrichtung.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAufgabeTextausrichtung.setBounds(10, 397, 364, 14);
		contentPane.add(lblAufgabeTextausrichtung);
		
		JButton btnLinksbndig = new JButton("Linksb\u00FCndig");
		btnLinksbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				schriftAusrichtungAendern("Linksb�ndig");
			}
		});
		btnLinksbndig.setBounds(10, 422, 103, 23);
		contentPane.add(btnLinksbndig);
		
		JButton btnZentriert = new JButton("zentriert");
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				schriftAusrichtungAendern("zentriert");
			}
		});
		btnZentriert.setBounds(123, 422, 125, 23);
		contentPane.add(btnZentriert);
		
		JButton btnRechtsbndig = new JButton("rechtsb\u00FCndig");
		btnRechtsbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				schriftAusrichtungAendern("Rechtsb�ndig");
			}
		});
		btnRechtsbndig.setBounds(259, 422, 115, 23);
		contentPane.add(btnRechtsbndig);
		
		JLabel lblAufgabeProgramm = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabeProgramm.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAufgabeProgramm.setBounds(10, 456, 364, 14);
		contentPane.add(lblAufgabeProgramm);
		
		JButton btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnExit.setBounds(10, 473, 364, 64);
		contentPane.add(btnExit);
	}

	
	protected void schriftAusrichtungAendern(String ausrichtung) {
		
		
		if (ausrichtung == "Linksb�ndig") {
			lblHierBitteText.setHorizontalAlignment(SwingConstants.LEFT);
			return;
		}
		if (ausrichtung == "zentriert") {
			lblHierBitteText.setHorizontalAlignment(SwingConstants.CENTER);
			return;
		}
		lblHierBitteText.setHorizontalAlignment(SwingConstants.RIGHT);
		
	}

	protected void groe�eAendern(Boolean gr��e�ndern) {
		int groesse = lblHierBitteText.getFont().getSize();
		String getFontName = lblHierBitteText.getFont().getFamily();
		if (gr��e�ndern)
			lblHierBitteText.setFont(new Font(getFontName, Font.PLAIN, groesse + 1));
		else
			lblHierBitteText.setFont(new Font(getFontName, Font.PLAIN, groesse - 1));	
	}

	protected void farbe�ndern(Color myColor) {
		this.lblHierBitteText.setForeground(myColor);
		
	}

	protected void textAusTextfeldEinlesen(String neuText) {
		this.lblHierBitteText.setText(neuText);
		
	}

	protected void buttonCourierNew_clicked() {
		this.lblHierBitteText.setFont(new Font("Courier New", Font.PLAIN, 12));
		
	}

	protected void buttonComicSansMS_clicked() {
		this.lblHierBitteText.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
		
	}

	protected void buttonArial_clicked() {
		this.lblHierBitteText.setFont(new Font("Arial", Font.PLAIN, 12));
		
	}

	protected void buttonWahl_clicked() {

				
	}

	protected void buttonStandart_clicked() {
		this.contentPane.setBackground(new Color(0xEEEEEE));
		
	}

	protected void buttonGelb_clicked() {
		this.contentPane.setBackground(Color.YELLOW);
		
	}

	protected void buttonBlau_clicked() {
		this.contentPane.setBackground(Color.BLUE);
		
	}

	protected void buttonGr�n_clicked() {
		this.contentPane.setBackground(Color.GREEN);
		
	}

	protected void buttonRot_clicked() {
		this.contentPane.setBackground(Color.RED);
		
	}
}
