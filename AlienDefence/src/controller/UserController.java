package controller;

import model.User;
import model.persistance.IUserPersistance;

/**
 * controller for users
 * @author Clara Zufall
 * TODO implement this class
 */
public class UserController {

	private IUserPersistance userPersistance; 
	
	
	public UserController(IUserPersistance userPersistance) {
		this.userPersistance = userPersistance;
	}
	
	public void createUser(User user) {
		
	}
	
	/**
	 * liest einen User aus der Persistenzschicht und gibt das Userobjekt zur�ck
	 * @param username eindeutige Loginname
	 * @return Userobjekt, null wenn der User nicht existiert
	 */
	public User readUser(String username) {
		User user=userPersistance.readUser(username);
		return user;
	}
	
	
	public void changeUser(User user) {
		
	}
	
	public IUserPersistance getUserPersistance() {
		return userPersistance;
	}

	public void setUserPersistance(IUserPersistance userPersistance) {
		this.userPersistance = userPersistance;
	}

	public void deleteUser(User user) {
		
	}
	
	public boolean checkPassword(String username, String passwort) {
		User user2=readUser(username);
		if (user2.getPassword().equals(passwort)) {
		return true;
		}
		return false;
	}
}
